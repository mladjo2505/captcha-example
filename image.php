<?php
/* start a session, to save the captcha string */
session_start();

function make_seed() {
    list($usec, $sec) = explode(' ', microtime());
    return $sec + $usec * 1000000;
}

function generate_random_str($length) {
    $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    $random_word = '';
    for ($i = 0; $i < $length; $i++) {
        $character_pos = rand(0, strlen($chars) - 1);
        $character = $chars[$character_pos];
        if (rand(0, 100) % 2 == 0) {
            $character = strtoupper($character);
        }
        $random_word .= $character;
    }
    return $random_word;
}

/* seed randomness with microseconds */
srand(make_seed());

/* generate random captcha string */
$random_strings = [];
$number_of_words = 2;
$max_length = 5;
for ($i = 0; $i < $number_of_words; $i++) {
    array_push($random_strings, generate_random_str($max_length));
}
$random_string = implode(' ', $random_strings);

/* save captcha string in the session */
$_SESSION['captcha'] = $random_string;

$character_width = 9;
$offset_x = 15;
$offset_y = 15;
$image_width = strlen($random_string) * $character_width + 2 * $offset_x;
$image_height = 45;
$text_size = 5;

/* create an image of string to display */
$image = imagecreate($image_width, $image_height);
/* set bg color to black */
$img_color = imagecolorallocate($image, 0, 0, 0);
/* set text color to white */
$txt_color = imagecolorallocate($image, 255, 255, 255);
/* draw string on image */
imagestring($image, $text_size, $offset_x, $offset_y, $random_string, $txt_color);

/* set content type and send image data */
header("Content-type: image/png");
imagepng($image);
imagedestroy($image);
exit;
