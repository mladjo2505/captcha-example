# Example CAPTCHA-ish form

This is an example of a simple CAPTCHA form built in PHP.

![](captcha.png)

To test out:

1. Get the code.
2. Run `php -S localhost:8000` in the code directory
3. Go to `localhost:8000` in your browser.
4. Profit :)
