<?php
/* start a session, to have access to the captcha string */
session_start();

$captcha_evaluated = false;
if (isset($_POST['captcha'])) {
    $captcha_input      = $_POST['captcha'];
    $session_captcha    = $_SESSION['captcha'];
    $success = strcmp($captcha_input, $session_captcha) == 0;
    $captcha_evaluated = true;
}
?>
<html>
<head>
    <title>CAPTCHA Examlple Form</title>
    <meta charset="UTF-8">
    <script>
        function validateForm() {
            var captcha_input = document.getElementById('captcha_input');
            var warning_span = document.getElementById('warning');
            if (captcha_input.value == '') {
                warning_span.innerHTML = 'You must populate all form fields!';
                return false;
            } else {
                return true;
            }
        }
        function reloadCaptcha() {
            var date = new Date();	
            var image_url = 'image.php?' + date.getTime();
            document.getElementById('captcha_image').src = image_url;
        }
    </script>
</head>
<body>
  <pre>
<?php
if ($captcha_evaluated) {
    echo $success ? 'SUCCESS!' : 'FAILURE!';
}
?>
  </pre>
  <form id="captcha_form" action="" method="post" onsubmit="return validateForm();">
    <img id="captcha_image" src="image.php"/>
    <br>
    <input type="button" onclick="reloadCaptcha();" value="Reload CAPTCHA">
    <br>
    <br>
    <input id="captcha_input" name="captcha" size="20" type="text" placeholder="Enter CAPTCHA">
    <br>
    <span id="warning"></span>
    <br>
    <input name="submit" type="submit" value="Send">
  </form>
  <br>
  <pre class="text-left">
<?php
if ($captcha_evaluated) {
    echo "You entered: '$captcha_input'\n";
    echo "CAPTCHA was: '$session_captcha'\n";
}
?>
  </pre>
</body>
</html>
